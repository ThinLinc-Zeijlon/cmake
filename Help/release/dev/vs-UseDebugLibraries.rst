vs-UseDebugLibraries
--------------------

* The :variable:`CMAKE_VS_USE_DEBUG_LIBRARIES` variable and corresponding
  :prop_tgt:`VS_USE_DEBUG_LIBRARIES` target property were added to explicitly
  control ``UseDebugLibraries`` indicators in ``.vcxproj`` files.
